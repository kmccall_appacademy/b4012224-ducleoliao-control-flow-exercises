# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.chars.select {|ch| ch == ch.upcase}.join("")
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  mid_idx = (str.length / 2).round
  return str[mid_idx] if (str.length).odd?
  return str[mid_idx-1..mid_idx] if (str.length).even?
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  sum = 0
  str.each_char {|ch| sum +=1 if VOWELS.include?(ch.downcase)}
  sum
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).inject(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  joined = ""
  arr.each do |el|
    joined << el
    joined << separator unless el == arr.last
  end
  joined
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str.chars.map.with_index {|ch, idx| (idx+1).even? ? ch.upcase : ch.downcase}.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str.split(" ").map {|word| word.length >= 5 ? word.reverse : word}.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  fb_arr = []
  (1..n).each do |int|
    if int % 3 == 0 && int % 5 == 0
      fb_arr << "fizzbuzz"
    elsif int % 5 == 0
      fb_arr << "buzz"
    elsif int % 3 == 0
      fb_arr << "fizz"
    else
      fb_arr << int
    end
  end
  fb_arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  rev_arr = []
  arr.each do |el|
    rev_arr.unshift(el)
  end
  rev_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num % 2 == 0 && num != 2 || num % 3 == 0 && num !=3 || num == 1
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
 (1..num).select {|int| num % int == 0}.sort
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
factors(num).select {|n| prime?(n)}
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).size
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  even_count = arr.select {|int| int.even?}.size
  odd_count = arr.select {|int| int.odd?}.size
  even_count > odd_count ? arr.select {|int| int.odd?}[0] : arr.select {|int| int.even?}[0]
end
